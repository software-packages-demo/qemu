# qemu

Machine emulator and virtualizer. https://qemu.org

# Booting with PXE
    qemu-system-x86_64 -enable-kvm -m 2G -kernel ipxe.lkrn
* [*iPXE*](https://en.m.wikipedia.org/wiki/IPXE) (WikipediA)
* [*Netboot*](https://wiki.archlinux.org/index.php/Netboot)
  (ArchWiki)
* [*Arch Linux Netboot*](https://www.archlinux.org/releng/netboot/) (Arch Linux)

## Demo for Linux distributions
* [apk](https://gitlab.com/apk-packages-demo/qemu-system-x86_64)
* [apt](https://gitlab.com/apt-packages-demo/qemu-system-x86)
* [hub.docker.com](https://gitlab.com/hub-docker-com-demo/qemu)

## [qemu-demo](https://gitlab.com/qemu-demo)
* [alpine-virt](https://gitlab.com/qemu-demo/alpine-virt)
* [ubuntu-cloud](https://gitlab.com/qemu-demo/ubuntu-cloud)

### qemu-kvm @ GitLab
* [apt](https://gitlab.com/apt-packages-demo/qemu-kvm)